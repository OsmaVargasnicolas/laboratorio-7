let miMatriz = [
    ["A", "B", "C"],
    ["D", "E", "F"],
    ["G", "H", "I"]
];

function armarTablero() {

    let tablero = document.getElementById("tablero");

    let pos = 1;
    for (let i = 0; i < 9; i++) {
        tablero.innerHTML = tablero.innerHTML +
            "<input type='text' class='casilla'  id='casilla" + pos + "' onClick='realizarJugada()'></input>";

        if ((i + 1) % 3 === 0) {
            tablero.innerHTML = tablero.innerHTML + "<br>";
        }
        pos++;
    }

    tablero.innerHTML = tablero.innerHTML +
        "<input class='boton' type='button' value='X' id='boton1' onclick='alternarBotones()'>";
    tablero.innerHTML = tablero.innerHTML +
        "<input class='boton' type='button' value='O' id='boton2' onclick='alternarBotones()'>";
}

function alternarBotones(triqui) {

    let boton1 = document.getElementById('boton1');
    let boton2 = document.getElementById('boton2');
    let casillas = document.getElementsByClassName('casilla');

    if (triqui) {
        boton1.disabled = true;
        boton2.disabled = true;
        casillas.disabled = true;
    } else {
        if (boton1.disabled === true) {
            boton1.disabled = false;
            boton2.disabled = true;
        } else {

            boton1.disabled = true;
            boton2.disabled = false;

        }
    }


}

function realizarJugada() {

    let miCuadro = event.target;
    let miMensaje = document.getElementById("mensaje");
    /* todo puesto en un if debido a que se puede dar que sea una x o una o y eso realizara todo 
    los procesos */

    if (!(miCuadro.value === "X" || miCuadro.value === "O")) 
    {
        let boton1 = document.getElementById('boton1');
        let valor;
        if (boton1.disabled === true) 
        {
            valor = 'X';
        } else 
        {
            valor = 'O';
        }
        miCuadro.value = valor;

    /* crear una variable llamada pos luego hacer dos for para recorrer la matriz 
    preguntar que realiza $ a la funciones e ir aumentando el poss para poder
    realizar los ciclos debido a que si no se aumenta apenas se da un click aparece que ganaste*/
        let pos = 1;

        for (let i = 0; i < 3; i++) 
        {
            for (let j = 0; j < 3; j++) 
            {
                miMatriz[i][j] = document.getElementById(`casilla${pos}`).value;
                pos++;
            }
        }

        //llamar la funcion para verificar el triqui
        triqui = verificarTriqui();
        //para alternar los botones para poder hacer que el triqui se altere los botones
        alternarBotones(triqui);
        /*para verificar y actualizar la posiciones del triqui y asi saber si estan siendo ocupadas o no
        dando una alerta si el triqui esta siendo usada y pondra la alerta cuadro ocupado todo esto gracias a la funcion 
        location.reload para que recargar cada vez mas despues retorna el true para dar la alerta de ganador*/
        if (triqui == true) {
            alert('¡Felicidades ganaste!');
            location.reload();
            return true;
            
        }

    } 
    else 
    {
        alert("¡Este cuadro ya está ocupado!");     
    }
    
}



function verificarTriqui() {

    let triqui = false;
    // SE CREARON DOS IF POR CADA DIAGONAL PARA MIRAR SI HAY UN TRIQUI CON X O CON O
    //Verificar si hay triqui en las filas
    for (let i = 0; i < 3; i++) {
        if (miMatriz[i][0] === 'O' && miMatriz[i][1] === 'O' && miMatriz[i][2] === 'O') 
        {
            triqui = true;
        }
        if (miMatriz[i][0] === 'X' && miMatriz[i][1] === 'X' && miMatriz[i][2] === 'X') 
        {
            triqui = true;
        }
    }

    //Verificar si hay triqui en las columnas
    for (let j = 0; j < 3; j++) {
        if (miMatriz[0][j] === 'O' && miMatriz[1][j] === 'O' && miMatriz[2][j] === 'O') 
        {
            triqui = true;
        }
        if (miMatriz[0][j] === 'X' && miMatriz[1][j] === 'X' && miMatriz[2][j] === 'X') 
        {
            triqui = true;
        }
    }

    // Para verificar si hay triqui en las diagonales
    if (miMatriz[0][0] === 'O' && miMatriz[1][1] === 'O' && miMatriz[2][2] === 'O') 
    {
        triqui = true;
    }
    if (miMatriz[0][0] === 'X' && miMatriz[1][1] === 'X' && miMatriz[2][2] === 'X') 
    {
        triqui = true;
    }

    if (miMatriz[0][2] === 'O' && miMatriz[1][1] === 'O' && miMatriz[2][0] === 'O') 
    {
        triqui = true;
    }

    if (miMatriz[0][2] === 'X' && miMatriz[1][1] === 'X' && miMatriz[2][0] === 'X') 
    {
        triqui = true;
    }

    return triqui;

}
